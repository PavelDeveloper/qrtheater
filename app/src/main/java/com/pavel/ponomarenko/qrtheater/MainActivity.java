package com.pavel.ponomarenko.qrtheater;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import com.pavel.ponomarenko.qrtheater.app.App;
import com.pavel.ponomarenko.qrtheater.data.AppDatabase;
import com.pavel.ponomarenko.qrtheater.data.TicketDao;
import com.pavel.ponomarenko.qrtheater.data.TicketData;
import com.pavel.ponomarenko.qrtheater.ui.DataAdapter;
import com.pavel.ponomarenko.qrtheater.ui.activities.QrScanActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import static com.pavel.ponomarenko.qrtheater.common.Constants.STATUS;

public class MainActivity extends AppCompatActivity {

    private RecyclerView mRecyclerView;
    private DataAdapter dataAdapter;
    private AppDatabase db;
    private TicketDao ticketDao;
    private TicketData ticketData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        db = App.getInstance().getDatabase();

        ticketDao = db.ticketDao();
        mRecyclerView = findViewById(R.id.ticketRecyclerView);
        dataAdapter = new DataAdapter();
        final LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.addItemDecoration(new DividerItemDecoration(mRecyclerView.getContext(),
                DividerItemDecoration.VERTICAL));
        mRecyclerView.setAdapter(dataAdapter);
        dataAdapter.setTickets(ticketDao.getAll());
    }

    private String createMD(TicketData ticketData) {

        return md5(ticketData.getTheater() + ticketData.getCode() +
                ticketData.getPlace() + ticketData.getRow() + ticketData.getDate()
                + ticketData.getTime() + ticketData.getSign());
    }

    public String md5(String s) {
        try {
            MessageDigest digest = java.security.MessageDigest.getInstance("MD5");
            digest.update(s.getBytes());
            byte[] messageDigest = digest.digest();
            StringBuilder hexString = new StringBuilder();
            for (byte b : messageDigest) hexString.append(Integer.toHexString(0xFF & b));

            return hexString.toString();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }

    public void pressScan(View view) {
        Intent intent = new Intent(this, QrScanActivity.class);
        startActivityForResult(intent, 1);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (data == null) {
            return;
        }
        ticketData = data.getParcelableExtra("ticket");
        if (!isAdded(ticketData)) {
            sendToServer(ticketData);
        } else {
            Toast.makeText(getApplicationContext(), getString(R.string.scanned), Toast.LENGTH_LONG).show();
        }

    }

    private Boolean isAdded(TicketData ticketData) {
        return ticketDao.getBySing(ticketData.getSign()) != null;
    }

    private void sendToServer(TicketData data) {
        JSONObject postData = new JSONObject();
        try {
            postData.put("theater", data.getTheater());
            postData.put("code", data.getCode());
            postData.put("place", data.getPlace());
            postData.put("row", data.getRow());
            postData.put("date", data.getDate());
            postData.put("time", data.getTime());
            postData.put("sign", createMD(data));

            new SendTicketData().execute("https://api-citysmart-1.botcreator.io/api/qrscan", postData.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    class SendTicketData extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {

            String data = "";

            HttpURLConnection httpURLConnection = null;
            try {

                httpURLConnection = (HttpURLConnection) new URL(params[0]).openConnection();
                httpURLConnection.setRequestMethod("POST");

                httpURLConnection.setDoOutput(true);

                DataOutputStream wr = new DataOutputStream(httpURLConnection.getOutputStream());
                wr.writeBytes("PostData=" + params[1]);
                wr.flush();
                wr.close();

                InputStream in = httpURLConnection.getInputStream();
                InputStreamReader inputStreamReader = new InputStreamReader(in);

                int inputStreamData = inputStreamReader.read();
                while (inputStreamData != -1) {
                    char current = (char) inputStreamData;
                    inputStreamData = inputStreamReader.read();
                    data += current;
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (httpURLConnection != null) {
                    httpURLConnection.disconnect();
                }
            }

            return data;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            try {
                JSONObject mainObject = new JSONObject(result);
                String status = mainObject.getString(STATUS);
                if (status.equals("true")) {
                    ticketDao.insert(ticketData);
                    dataAdapter.setTickets(ticketDao.getAll());
                    Toast.makeText(getApplicationContext(), getString(R.string.added), Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(getApplicationContext(), getString(R.string.not_validated), Toast.LENGTH_LONG).show();
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }
}
