package com.pavel.ponomarenko.qrtheater.common;

public class Constants {
    public static final String THEATER = "theater";
    public static final String CODE = "code";
    public static final String PLACE = "place";
    public static final String ROW = "row";
    public static final String DATE = "date";
    public static final String TIME = "time";
    public static final String SING = "sign";
    public static final String STATUS = "status";
}
