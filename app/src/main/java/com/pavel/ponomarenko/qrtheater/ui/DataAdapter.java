package com.pavel.ponomarenko.qrtheater.ui;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.pavel.ponomarenko.qrtheater.R;
import com.pavel.ponomarenko.qrtheater.data.TicketData;

import java.util.ArrayList;
import java.util.List;

public class DataAdapter extends RecyclerView.Adapter<DataAdapter.ViewHolder> {

    private List<TicketData> tickets = new ArrayList<>();
    private View view;

    @Override
    public DataAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull DataAdapter.ViewHolder holder, int position) {

        TicketData ticket = tickets.get(position);
        String theaterStr = view.getContext().getString(R.string.theater) + " " + ticket.getTheater();
        String codeStr = view.getContext().getString(R.string.code) + " " + ticket.getCode();
        String placeStr = view.getContext().getString(R.string.place) + " " + ticket.getPlace();
        String rowStr = view.getContext().getString(R.string.row) + " " + ticket.getRow();
        String dateStr = view.getContext().getString(R.string.date) + " " + ticket.getDate();
        String timeStr = view.getContext().getString(R.string.time) + " " + ticket.getTime();
        String signStr = view.getContext().getString(R.string.sign) + " " + ticket.getSign();

        holder.theater.setText(theaterStr);
        holder.code.setText(codeStr);
        holder.place.setText(placeStr);
        holder.row.setText(rowStr);
        holder.date.setText(dateStr);
        holder.time.setText(timeStr);
        holder.sign.setText(signStr);
    }

    @Override
    public int getItemCount() {
        return tickets.size();
    }

    public void setTickets(List<TicketData> ticketsData) {
        tickets.clear();
        tickets.addAll(ticketsData);
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        final TextView theater, code, place, row, date, time, sign;
        ViewHolder(View view){
            super(view);
            theater = view.findViewById(R.id.theater);
            code =  view.findViewById(R.id.code);
            place =  view.findViewById(R.id.place);
            row =  view.findViewById(R.id.row);
            date =  view.findViewById(R.id.date);
            time =  view.findViewById(R.id.time);
            sign =  view.findViewById(R.id.sign);
        }
    }
}
