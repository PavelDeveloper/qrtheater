package com.pavel.ponomarenko.qrtheater.data;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

@Dao
public interface TicketDao {

    @Query("SELECT * FROM ticketData")
    List<TicketData> getAll();

    @Query("SELECT * FROM ticketData WHERE sign = :sing")
    TicketData getBySing(String sing);

    @Insert
    void insert(TicketData employee);

    @Update
    void update(TicketData employee);

    @Delete
    void delete(TicketData employee);
}
