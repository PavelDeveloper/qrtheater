package com.pavel.ponomarenko.qrtheater.data;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.os.Parcel;
import android.os.Parcelable;

@Entity
public class TicketData implements Parcelable {

    @PrimaryKey(autoGenerate = true)
    public long id;
    private String theater;
    private String code;
    private String place;
    private String row;
    private String date;
    private String time;
    private String sign;

    public TicketData(Parcel in) {
        theater = in.readString();
        code = in.readString();
        place = in.readString();
        row = in.readString();
        date = in.readString();
        time = in.readString();
        sign = in.readString();
    }

    public static final Creator<TicketData> CREATOR = new Creator<TicketData>() {
        @Override
        public TicketData createFromParcel(Parcel in) {
            return new TicketData(in);
        }

        @Override
        public TicketData[] newArray(int size) {
            return new TicketData[size];
        }
    };

    public TicketData() {

    }

    public String getSign() {
        return sign;
    }

    public void setSign(String sign) {
        this.sign = sign;
    }

    public String getTheater() {
        return theater;
    }

    public void setTheater(String theater) {
        this.theater = theater;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public String getRow() {
        return row;
    }

    public void setRow(String row) {
        this.row = row;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(theater);
        dest.writeString(code);
        dest.writeString(place);
        dest.writeString(row);
        dest.writeString(date);
        dest.writeString(time);
        dest.writeString(sign);
    }
}
