package com.pavel.ponomarenko.qrtheater.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.google.zxing.Result;
import com.pavel.ponomarenko.qrtheater.R;
import com.pavel.ponomarenko.qrtheater.data.TicketData;

import me.dm7.barcodescanner.zxing.ZXingScannerView;

import static com.pavel.ponomarenko.qrtheater.common.Constants.CODE;
import static com.pavel.ponomarenko.qrtheater.common.Constants.DATE;
import static com.pavel.ponomarenko.qrtheater.common.Constants.PLACE;
import static com.pavel.ponomarenko.qrtheater.common.Constants.ROW;
import static com.pavel.ponomarenko.qrtheater.common.Constants.SING;
import static com.pavel.ponomarenko.qrtheater.common.Constants.THEATER;
import static com.pavel.ponomarenko.qrtheater.common.Constants.TIME;

public class QrScanActivity extends AppCompatActivity implements ZXingScannerView.ResultHandler {

    private ZXingScannerView mScannerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qr_scan);
        mScannerView = new ZXingScannerView(this);
        setContentView(mScannerView);
    }

    @Override
    public void onResume() {
        super.onResume();
        mScannerView.setResultHandler(this);
        mScannerView.startCamera();
    }

    @Override
    public void onPause() {
        super.onPause();
        mScannerView.stopCamera();
    }

    @Override
    public void handleResult(Result rawResult) {

        Intent intent = new Intent();

        if (validTicket(rawResult)) {
            intent.putExtra("ticket", createTicketData(rawResult));
            setResult(RESULT_OK, intent);
        } else {
            Toast.makeText(this, getString(R.string.not_ticket), Toast.LENGTH_LONG).show();
            setResult(RESULT_CANCELED, intent);
        }
        finish();
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

    private TicketData createTicketData(Result rawResult) {
        TicketData data = new TicketData();
        String ticketData = rawResult.getText();
        String[] arrOfStr = ticketData.split("&", 7);
        for (String s : arrOfStr) {
            if (s.startsWith(THEATER)) {
                data.setTheater(getTicketData(s));
            } else if (s.startsWith(CODE)) {
                data.setCode(getTicketData(s));
            } else if (s.startsWith(PLACE)) {
                data.setPlace(getTicketData(s));
            } else if (s.startsWith(ROW)) {
                data.setRow(getTicketData(s));
            } else if (s.startsWith(DATE)) {
                data.setDate(getTicketData(s));
            } else if (s.startsWith(TIME)) {
                data.setTime(getTicketData(s));
            }
            else if (s.startsWith(SING)) {
                data.setSign(getTicketData(s));
            }else break;
        }
        return data;
    }

    private String getTicketData(String s) {
        int eq = s.lastIndexOf("=");
        char[] buf = new char[(s.length() - 1) - eq];
        s.getChars(eq+1, s.length(), buf, 0);
        return new String(buf);
    }

    private Boolean validTicket(Result rawResult) {
        String result = rawResult.getText();
        return result.contains(THEATER) && result.contains(CODE) && result.contains(PLACE) &&
                result.contains(ROW) && result.contains(DATE) && result.contains(TIME) &&
                result.contains(SING);
    }
}
